<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>CorsairHub - Home</title>
		<meta name="description" content="">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/web1/img/favicon.ico">
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>
	<body>
		<header>
			<?php
				require $_SERVER['DOCUMENT_ROOT'].'/web1/header.php';
			?>
		</header>

		<main>
			<?php
			$user = 'xx';
			$password = 'xx';
			$con = new PDO('mysql:host=studmysql01.fhict.local;dbname=dbi418108', $user, $password);
			$sql = "select * from movies where genres like '%Action%';";
			$statment = $con->prepare($sql);
			$statment->execute();
			$result = $statment->fetchAll();
			$max = 6;

			if (count($result)>0) {
				function getRating($stars){
					if ($stars >= 4.7) {
						echo"<i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>";
					}
					else if ($stars < 4.7 && $stars >= 4.3) {
						echo"<i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star-half-alt'></i>";
					}
					else if ($stars < 4.3 && $stars >= 3.9) {
						echo"<i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'far fa-star'></i>";
					}
					else if ($stars < 3.9 && $stars >= 3.4) {
						echo"<i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star-half-alt'></i>
								 <i class= 'far fa-star'></i>";
					}
					else if ($stars < 3.4 && $stars >= 2.9) {
						echo"<i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>";
					}
					else if ($stars < 2.9 && $stars >= 2.4) {
						echo"<i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star-half-alt'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>";
					}else if ($stars < 2.4 && $stars >= 1.9) {
						echo"<i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>";
					}else if ($stars < 1.9 && $stars >= 1.4) {
						echo"<i class= 'fas fa-star'></i>
								 <i class= 'fas fa-star-half-alt'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>";
					}else if ($stars < 1.4 && $stars >= 0.9) {
						echo"<i class= 'fas fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>";
					}else if ($stars < 0.9 && $stars >= 0.4) {
						echo"<i class= 'fas fa-star-half-alt'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>";
					}
					else if ($stars < 0.4) {
						echo"<i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>
								 <i class= 'far fa-star'></i>";
					}
				}
				echo "<section class='category'>";
				echo "<h2>Action</h2>
				<h3><a href='categories/action/index.php'>see more</a></h3>";
				$foundMovies = 0;
				for ($i=0; $i < count($result); $i++) {
					if($foundMovies <= $max){
						echo "<article>
							<img src='".$result[$i]['posterLink']."' alt=''>
							<a href='/web1/movie.php?id=".$result[$i]['imdbID']."'>
								<div class='overlay'>
									<p class='movie-title'>".$result[$i]['title']."</p>
									<p>".$result[$i]['tagline']."</p>
								</div>
							</a>";
							$stars = $result[$i]['rating']/2;
							getRating($stars);
						echo"</article>";
					}
					else {
						$foundMovies = 0;
						break;
					}
					$foundMovies++;
				}
				echo "</section>";
			}
			$sql = "select * from movies where genres like '%Comedy%';";
			$statment = $con->prepare($sql);
			$statment->execute();
			$result = $statment->fetchAll();

			if (count($result)>0) {
				echo "<section class='category'>";
				echo "<h2>Comedy</h2>
				<h3><a href='categories/comedy/index.php'>see more</a></h3>";
				$foundMovies = 0;
				for ($i=0; $i < count($result); $i++) {
					if($foundMovies <= $max){
						echo "<article>
							<img src='".$result[$i]['posterLink']."' alt=''>
							<a href='/web1/movie.php?id=".$result[$i]['imdbID']."'>
								<div class='overlay'>
									<p class='movie-title'>".$result[$i]['title']."</p>
									<p>".$result[$i]['tagline']."</p>
								</div>
							</a>";
							$stars = $result[$i]['rating']/2;
							getRating($stars);
						echo "</article>";
					}
					else {
						$foundMovies = 0;
						break;
					}
					$foundMovies++;
				}
				echo "</section>";
			}
			$sql = "select * from movies where genres like '%Thriller%';";
			$statment = $con->prepare($sql);
			$statment->execute();
			$result = $statment->fetchAll();

			if (count($result)>0) {
				echo "<section class='category'>";
				echo "<h2>Thriller</h2>
				<h3><a href='categories/thriller/index.php'>see more</a></h3>";
				$foundMovies = 0;
				for ($i=0; $i < count($result); $i++) {
					if($foundMovies <= $max){
						echo "<article>
							<img src='".$result[$i]['posterLink']."' alt=''>
							<a href='/web1/movie.php?id=".$result[$i]['imdbID']."'>
								<div class='overlay'>
									<p class='movie-title'>".$result[$i]['title']."</p>
									<p>".$result[$i]['tagline']."</p>
								</div>
							</a>";
							$stars = $result[$i]['rating']/2;
							getRating($stars);
						echo "</article>";
					}
					else {
						$foundMovies = 0;
						break;
					}
					$foundMovies++;
				}
				echo "</section>";
			}
			$sql = "select * from movies where genres like '%Horror%';";
			$statment = $con->prepare($sql);
			$statment->execute();
			$result = $statment->fetchAll();

			if (count($result)>0) {
				echo "<section class='category'>";
				echo "<h2>Horror</h2>
				<h3><a href='categories/horror/index.php'>see more</a></h3>";
				$foundMovies = 0;
				for ($i=0; $i < count($result); $i++) {
					if($foundMovies <= $max){
						echo "<article>
							<img src='".$result[$i]['posterLink']."' alt=''>
							<a href='/web1/movie.php?id=".$result[$i]['imdbID']."'>
								<div class='overlay'>
									<p class='movie-title'>".$result[$i]['title']."</p>
									<p>".$result[$i]['tagline']."</p>
								</div>
							</a>";
							$stars = $result[$i]['rating']/2;
							getRating($stars);
						echo"</article>";
					}
					else {
						$foundMovies = 0;
						break;
					}
					$foundMovies++;
				}
				echo "</section>";
			}
			$sql = "select * from movies where genres like '%Anime%' or genres like '%Animation%';";
			$statment = $con->prepare($sql);
			$statment->execute();
			$result = $statment->fetchAll();

			if (count($result)>0) {
				echo "<section class='category'>";
				echo "<h2>Anime</h2>
				<h3><a href='categories/anime/index.php'>see more</a></h3>";
				$foundMovies = 0;
				for ($i=0; $i < count($result); $i++) {
					if($foundMovies <= $max){
						echo "<article>
							<img src='".$result[$i]['posterLink']."' alt=''>
							<a href='/web1/movie.php?id=".$result[$i]['imdbID']."'>
								<div class='overlay'>
									<p class='movie-title'>".$result[$i]['title']."</p>
									<p>".$result[$i]['tagline']."</p>
								</div>
							</a>";
							$stars = $result[$i]['rating']/2;
							getRating($stars);
						echo"</article>";
					}
					else {
						$foundMovies = 0;
						break;
					}
					$foundMovies++;
				}
				echo "</section>";
			}
			?>
		</main>
		<footer>
			<?php
				require ($_SERVER['DOCUMENT_ROOT'].'/web1/footer.php');
			?>
		</footer>
		<script src="/web1/ajaxsearch.js"></script>
	</body>
</html>
