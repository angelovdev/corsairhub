<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>CorsairHub - Register</title>
		<meta name="description" content="">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/web1/img/favicon.ico">
		<link rel="stylesheet" type="text/css" href="../style.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>
	<body>
		<header>
			<?php
				require $_SERVER['DOCUMENT_ROOT'].'/web1/header.php';
			?>
		</header>

		<main>

			<?php 
				if (isset($_SESSION['logged'])){
					echo "<h1 style='text-align:center; margin: 350px 0;'>Already logged in!</h1>";
					header('Refresh: 1; url=/index.php');
				}
				else 
				{
					echo 
					"<section id='form'>
						<h1>Register</h1>
						<form action='../register.php' method='POST'>
							<p>
								<label for='bsn'>BSN:</label>
								<input type='text' name='bsn' id='bsn'>
							</p>
							<p>
								<label for='pass'>Password:</label>
								<input type='password' name='pass' id='pass'>
							</p>
							<p>
								<label for='pass2'>Confirm password:</label>
								<input type='password' name='pass2' id='pass2'>
							</p>";
					if(isset($_COOKIE['error'])) {
						echo "<p><span style='color: red;'>".$_COOKIE['error']."</span></p>";
						setcookie('error', '', time() - 3600, '/');
					}
							
					echo "<p>
								<input type='checkbox' name='years' id='years'>
								<label for='years'>I am over 18 years old</label>
								<input type='submit' value='Register' id='submit'>
							</p>
						</form>
					</section>";
				}
			?>

			
		</main>
		<footer>
			<?php
				require ($_SERVER['DOCUMENT_ROOT'].'/web1/footer.php');
			?>
		</footer>
		<script src="/ajaxsearch.js"></script>
		<script type="text/javascript" src="/web1/form-validation.js"></script>
	</body>
</html>