<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
	header('Access-Control-Allow-Credentials: true');
	$query = $_GET['query'];

	function get_http_response_code($url) {
		$headers = get_headers($url);
		return substr($headers[0], 9, 3);
	}

	$uri = 'https://www.omdbapi.com/?apikey=e49b4e3d&page=1&type=movie&s='.$query;
	$reqPrefs['http']['method'] = 'GET';
	$stream_context = stream_context_create($reqPrefs);

	if(get_http_response_code($uri) != "200"){
		
	}
	else
	{
		try {
			$response = file_get_contents($uri, false, $stream_context);
			$data = json_decode($response, true);
			if ($data['Response'] == "True"){
				$result = $data['Search'];
				if (count($result) > 0){
					echo "<h2>Unavailable movies:</h2>";
					echo "<section class='movies'>";
					for ($i=0; $i < count($result); $i++) {

						echo "<article>
								<h3>".$result[$i]['Title']."</h3>
								<a href='/web1/movie.php?id=".$result[$i]['imdbID']."'><img src='".$result[$i]['Poster']."' alt='Movie poster of ".$result[$i]['Title']."'></a>
							</article>";
					}
					echo "</section>";
				}
				else 
				{
					echo "<h2>No results found in unavailable movies.</h2>";
				}
			}
		}
		catch (Exception $e){
			
		}
		
	}

