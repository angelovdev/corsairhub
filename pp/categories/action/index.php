<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<title>CorsairHub - Action</title>
		<meta name="description" content="">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/web1/img/favicon.ico">
		<link rel="stylesheet" href="../../style.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>
	<body>
		<header>
			<?php
				require $_SERVER['DOCUMENT_ROOT'].'/web1/header.php';
			?>
		</header>
		<main>
			<h1 class="name">Action</h1>
			<section class="movies">
				<?php

					$category = explode('/', $_SERVER['REQUEST_URI'])[3];

					$user = 'xx';
					$password = 'xx';

					$con = new PDO('mysql:host=studmysql01.fhict.local;dbname=dbi418108', $user, $password);

					$sql = "select * from movies where genres like '%$category%'";

					$statement = $con->prepare($sql);

					$statement->execute();

					$result = $statement->fetchAll();

					if (count($result) > 0){
						// echo ($result[0]['hash']);
						for ($i=0; $i < count($result); $i++) {
							$title = $result[$i]['title'];
							$imdbID = $result[$i]['imdbID'];
							$poster = $result[$i]['posterLink'];
							$rating = round($result[$i]['rating']/2, 1);

							echo "
							<article>
								<h3>".$title."</h3>
								<a href='/web1/movie.php?id=".$result[$i]['imdbID']."'><img src='".$poster."' alt='Movie poster of ".$title."'></a>
								<p>".$rating."/5</p>
							</article>
							";
						}
					}
					else
					{
						echo "<h3>No movies found!</h3>";
					}
				?>
			</section>
		</main>
		<footer>
			<?php
				require ($_SERVER['DOCUMENT_ROOT'].'/web1/footer.php');
			?>
		</footer>
		<script src="/web1/ajaxsearch.js"></script>
	</body>
</html>
