window.onscroll = function() {scrollFunction()};

function scrollFunction() {
	if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
		const links = document.querySelectorAll('ul li a');

		links.forEach( el => {
			el.style.padding = "20px 10px";
		});

		const logo = document.querySelectorAll('header a img');

		logo.forEach( el => {
			el.style.height = "59px";
		});

		const searchResultsDiv = document.querySelectorAll('.search-results');

		searchResultsDiv.forEach (el => {
			el.style.top = "39px";
		});

		const searchResult = document.querySelectorAll('.result a');

		searchResult.forEach( el => {
			el.style.padding = "19.5px 0px";
		});

		const subLinks = document.querySelectorAll('ul li ul li a');

		subLinks.forEach( el => {
			el.style.padding = "20px 0px";
		});
	} else {
		const links = document.querySelectorAll('ul li a');

		links.forEach( el => {
			el.style.padding = "30px 20px";
		});

		const logo = document.querySelectorAll('header a img');

		logo.forEach( el => {
			el.style.height = "78px";
		});

		const searchResultsDiv = document.querySelectorAll('.search-results');

		searchResultsDiv.forEach (el => {
			el.style.top = "48px";
		});

		const searchResult = document.querySelectorAll('.result a');

		searchResult.forEach( el => {
			el.style.padding = "30px 0px";
		});

		const subLinks = document.querySelectorAll('ul li ul li a');

		subLinks.forEach( el => {
			el.style.padding = "30px 0px";
		});
	}
} 