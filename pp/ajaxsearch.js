document.getElementsByClassName("search-field")[0].addEventListener("mouseover", show);
document.getElementsByTagName("header")[0].addEventListener("mouseleave", hide);
document.getElementsByTagName("main")[0].addEventListener("click", hide);
document.getElementsByTagName("footer")[0].addEventListener("click", hide);
var timeout;
document.getElementById("search").addEventListener("input", function(e){
	clearTimeout(timeout);
	timeout = setTimeout(search, 500);
});

function search(){
	var query = document.getElementById("search").value.trim();
	document.getElementsByClassName('search-results')[0].innerHTML = "<article class='result showall'>Loading...</article>";
	if (query.length > 0){
		document.getElementsByClassName('search-results')[0].style.visibility = "visible";
		var request = new XMLHttpRequest();
		request.open("GET", "/web1/ajaxsearch.php?query="+query, true);
		request.setRequestHeader('Access-Control-Allow-Origin','*');
		request.onload = function () {
			document.getElementsByClassName('search-results')[0].innerHTML = this.responseText;
			if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
				const searchResult = document.querySelectorAll('.result a');

				searchResult.forEach( el => {
					el.style.padding = "19.5px 0px";
				});
			}
			else {
				const searchResult = document.querySelectorAll('.result a');

				searchResult.forEach( el => {
					el.style.padding = "30px 0px";
				});
			}
		}
		request.send();
	}
	else {
		document.getElementsByClassName('search-results')[0].innerHTML = "";
		document.getElementsByClassName('search-results')[0].style.visibility = "hidden";
	}
	
}

function hide(){
	document.getElementsByClassName('search-results')[0].style.visibility = "hidden";
}

function show(){
	document.getElementsByClassName('search-results')[0].style.visibility = "visible";
	if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
		const searchResult = document.querySelectorAll('.result a');

		searchResult.forEach( el => {
			el.style.padding = "19.5px 0px";
		});
	}
	else {
		const searchResult = document.querySelectorAll('.result a');

		searchResult.forEach( el => {
			el.style.padding = "30px 0px";
		});
	}
}