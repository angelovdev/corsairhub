<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>CorsairHub - Add Movie</title>
		<meta name="description" content="">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/web1/img/favicon.ico">
		<link rel="stylesheet" type="text/css" href="/web1/style.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>
	<body>
		<header>
			<?php
				require ($_SERVER['DOCUMENT_ROOT'].'/web1/header.php');
			?>
		</header>

		<main>
			<?php
				if (isset($_SESSION['logged']) == true){
					$id = "";
					if (count(explode('=', $_SERVER['QUERY_STRING'])) > 1){
						$id = explode('=', $_SERVER['QUERY_STRING'])[1];
					}

					function get_http_response_code($url) {
						$headers = get_headers($url);
						return substr($headers[0], 9, 3);
					}


					$uri = 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=79cbfed63302bc4c9b95acbc95ef1305';
					$reqPrefs['http']['method'] = 'GET';
					$stream_context = stream_context_create($reqPrefs);

					if(get_http_response_code($uri) != "200"){
						
					}
					else
					{
						try {
							$response = file_get_contents($uri, false, $stream_context);
							$data = json_decode($response, true);
							if (!isset($data['status_code'])){
								$year = explode('-', $data['release_date'])[0];
								$title = $data['title'];
								$movieFound = true;
							}
							else {

							}
						}
						catch (Exception $e){
							
						}
						
					}


					echo 
					"<section id='form'>
						<h1>Add a new movie</h1>
						<form action='newmovie.php' method='post'>
							<p>
								<label for='imdbID'>ID:</label>
								<input type='text' name='imdbID' id='imdbID' value='".$id."'>
							</p>
							<p>
								<label for='hash'>Hash:</label>
								<input type='text' name='hash' id='hash'>
							</p>
							<p>
								<input type='submit' name='add' value='Add'></input>
							</p>
						</form>
					</section>";
				}
				else 
				{
					echo "<h1 style='text-align:center; margin: 350px 0;'>You need to login first!</h1>";
					header('Refresh: 1; url=login/index.php');
				}
			?>
		</main>
		<footer>
			<?php
				require ($_SERVER['DOCUMENT_ROOT'].'/web1/footer.php');
			?>
		</footer>
		<script src="/web1/ajaxsearch.js"></script>
	</body>
</html>
