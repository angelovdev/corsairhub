document.getElementById("search").addEventListener("keyup", search);
document.addEventListener("click", clear);
document.getElementById("search").addEventListener("focusin", search);

function search(){
	var query = document.getElementById("search").value.trim();
	if (query.length > 0){
		var request = new XMLHttpRequest();
		request.open("GET", "ajaxsearch.php?query="+query, true);
		request.onload = function () {
			document.getElementsByClassName('search-results')[0].innerHTML = this.responseText;
		}
		request.send();
	}
	else {
		document.getElementsByClassName('search-results')[0].innerHTML = "";
	}
	
}

function clear(){
	document.getElementsByClassName('search-results')[0].innerHTML = "";
}